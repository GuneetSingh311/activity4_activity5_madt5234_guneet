import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


/* Requirements to build this APP:
 * function name scream(name) should accept name of the person and perform follwing requiremens:
 * 1. if only one person name is given it should return "(name)  is amazing.
 * 2. if no name is given it should return "You is amazing".
 * 3. if all uppercase letters are given it should return "(NAME) IS AMAZING".
 * 4. if  two names are given it should return name1 and name(2) are amazing.
 * 5. if more than two names are given it should add comma with name and "and" before last name 
 * 6. if  names are given in uppercase and regular it should return name1 and name2 are amazing. NAME3 ALSO!. 
 */

class TestTribalScreaming {

	
	///////////////// Requirement1: TEST CASE////////////////
	@Test
	void testFunctionForOnePerson() {
	//// Req1: Green-phase: Test Case Pass
		
		TribalScreaming ts = new TribalScreaming();
		String name[] = {"Jigisha"}; // Creating an array name and storing one value
		String rs = ts.scream(name); // passing array in the function.
		assertEquals("Jigisha is amazing",rs);
		}
	
	///// Requirement2: TEST CASE////////////////
	@Test
	void testFunctionForNoName() 
	{   // Req2: Green-phase: Test Case Pass afterpassing null in function and changing code.
	   
		
		 TribalScreaming ts = new TribalScreaming();
		 String rs = ts.scream(null); // passing null in the function.
		 assertEquals("You is amazing",rs);
		}
	
	
	///// Requirement3: Test Case/////////
	@Test 
	void testFunctionForUpperCase() 
	{ 	// Req3: Green-phase: Test Case Pass
		
		
		TribalScreaming ts = new TribalScreaming();
		String name[] = {"PETER"};  // Creating array and Storing name in uppercase.
		String rs = ts.scream(name); // passing array in the function.
		assertEquals("PETER IS AMAZING",rs); 
		
		}
	
	/////////// Requirement4: Test Case///////
	@Test
	void testFunctionForTwoPeople() {
		// Req4: Green-phase: Test Case Pass
		TribalScreaming ts = new TribalScreaming();
		String name[] = {"Peter","Jigisha"};
		String rs = ts.scream(name);
		assertEquals("Peter and Jigisha are amazing",rs);
	}
	
	/////////// Requirement5: Test Case//////
	@Test
	void testFunctionForMoreThanTwoPeople() 
	{	// Req5: Green-phase:Test Case Pass as expected output equal to actual output.
		// Test Case Pass when more than 2 inputs are given.
		TribalScreaming ts = new TribalScreaming();
		String name[] = {"Peter","Jigisha","Marcos","Pritesh"};
		String rs = ts.scream(name);
		assertEquals("Peter,Jigisha,Marcos, and Pritesh are amazing",rs);
	}
	
	
	////////// Requirement6: Test Case/////
	
	@Test
	void testFunctionForShoutingAtAlotOfPeople()
	{
		
	// Req6:RED-phase: Test Case  fails as Expected output is not equal to actual output.
	// Req1,Req2,Req3,Req4,Req5 Test Case Pass.
		TribalScreaming ts = new TribalScreaming();
		String name[] = {"Peter","MARCOS"};
		String rs = ts.scream(name);
		assertEquals("Peter is amazing. MARCOS ALSO!",rs);
	}
	}
